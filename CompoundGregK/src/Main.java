/**
 * The user is asked to chose a compound interest equation, and prompted to then input the necessary 
 * data for the equation to run. Information is fed into the program by use of the scanner.
 * Calculations take place in the Calculations class. This is where the answers are also returned.   
 * @author Greg Kline
 * Project Due: February 13th, 2017
 */
import java.util.Scanner;

public class Main {
	static Calculations calc = new Calculations();
	static Scanner myChoice = new Scanner(System.in);

	public static void main(String[] args) {
		
		/*
		 * Print out directions for choosing equation
		 */
		
		System.out.println("Which equation would you like to use? " + "\n" + 
				"Type the corresponding number: " + "\n" + "1: Calculate Principal" + "\n" 
				+ "2: Calculate Total Amount after Compounding" + "\n" + "3: Calculate Total Interest" + "\n" +
				"4: Calculate Annual Interest Rate" + "\n" + "5: Calculate Time in Years"
				+ "\n" + "(Percents are represented in decimal form, enter data in the order asked.)");
		
		decision(myChoice.nextInt());
		

	}
	
	public static void decision(int x)
	{
	/*
	 * Decide which equation to run and what directions to print
	 */
		if(x == 1)
		{
			calc.setChoice(1);
			calc.directions();
			float answer = calc.findPrincipal(myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat());
			System.out.println(answer);
		}
		else if(x == 2)
		{
			calc.setChoice(2);
			calc.directions();
			float answer = calc.findAmount(myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat());
			System.out.println(answer);
		}
		else if(x == 3)
		{
			calc.setChoice(3);
			calc.directions();
			float answer = calc.findTotalInterest(myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat());
			System.out.println(answer);	
		}
		else if(x == 4)
		{
			calc.setChoice(4);
			calc.directions();
			float answer = calc.findAnnualInterest(myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat());
			System.out.println(answer);	
		}
		else if(x == 5)
		{
			calc.setChoice(5);
			calc.directions();
			double answer = calc.findTime(myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat(), myChoice.nextFloat());
			System.out.println(answer);	
		}
		/*
		 * Close program if values below or above 1-5 are inputted.
		 */
		else
		{
			System.out.println("Invalid number! Exiting...");
			System.exit(0);
		}
		
	}

}
