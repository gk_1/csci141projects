
public class Calculations {
	private static int equation;
	
	/**
	 * Constructor
	 */
	public Calculations()
	{

	}
	
	
	public static float findPrincipal(float a, float r, float m, float t )
	{
		float val1 = r/m;
		float val2 = val1 + 1;
		float val3 = m*t;
		float val4 = (float)Math.pow(val2,val3);
		float answer = a/val4;
		System.out.println("The principal amount is: ");
		return answer;
		
	}
	
	public static float findAmount(float p, float r, float m, float t)
	{
		float val1 = r/m;
		float val2 = val1 + 1;
		float val3 = m*t;
		float val4 = (float)Math.pow(val2,val3);
		float answer = p*val4;
		System.out.println("The amount is: ");
		return answer;
		
	}
	
	public static float findTotalInterest(float p, float r, float m, float t)
	{
		float val1 = r/m;
		float val2 = val1 + 1;
		float val3 = m*t;
		float val4 = (float)Math.pow(val2,val3);
		float answer = p * (val4-1);
		System.out.println("The total interest rate is: ");
		return answer;
		
	}
	
	public static float findAnnualInterest(float p, float a, float m, float t)
	{
		float val1 = a/p;
		float val2 = m*t;
		float val3 = 1/(val2);
		float val4 = (float)Math.pow(val1,val3);
		float val5 = val4 - 1;
		float answer = m * val5; 
		System.out.println("The annual interest rate is: ");
		return answer;
		
	}
	
	public static double findTime(float p, float a, float m, float r)
	{
		float val1 = a/p;
		double val2 = Math.log(val1);
		float val3 = r/m;
		float val4 = 1 + val3;
		double val5 = Math.log(val4);
		double val6 = m * val5;
		double answer = val2/val6;
		System.out.println("The time in years is: ");
		return answer;
		
	}
	

	
	public void directions()
	{
		if(getChoice() == 1)
		{
			System.out.println("Enter the amount after compounding, annual interest rate (5% would be 0.05), "
					+ "number of times compounded annually, and the number" + "\n" + "of times per year for compounding"
					+ " pressing enter after every value.");
		}
		else if (getChoice() == 2)
		{
			System.out.println("Enter the principal amount, annual interest rate (5% would be 0.05), "
					+ "number of times compounded annually, and the number" + "\n" + "of times per year for compounding" 
					+ " pressing enter after every value.");
		}
		else if (getChoice() == 3)
		{
			System.out.println("Enter the principal amount, annual interest rate (5% would be 0.05), "
					+ "number of times compounded annually, and the number" + "\n" + "of times per year for compounding" 
					+ " pressing enter after every value.");
		}
		else if (getChoice() == 4)
		{
			System.out.println("Enter the principal amount, amount after compounding, "
					+ "number of times compounded annually, and the number" + "\n" + "of times per year for compounding" 
					+ " pressing enter after every value.");
		}
		else if (getChoice() == 5)
		{
			System.out.println("Enter the principal amount, amount after compounding, "
					+ "number of times compounded annually, and the annual interest rate (5% would be 0.05). " + "\n" + "Press enter after every value.");
		}
	}
	
	/*
	 * Setters and getters
	 */
	
	public static void setChoice(int x1)
	{
		equation = x1;
	}
	
	public static int getChoice()
	{
		return equation;
	}

}
