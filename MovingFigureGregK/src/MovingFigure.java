/**
 * The user can input 3 different lines of text and choose their positions on a 500x500 window.
 * Windows will also appear asking the user to choose the x and y coordinates of a cactus made
 * with Graphics. 
 * 
 * @author Greg Kline
 * Project Due: February 6th, 2017
 */

import java.awt.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class MovingFigure extends JFrame{
	
	public static int x1, x2 = 0;
	public static int y1, y2 = 0;
	public static String message1 = " ";
	public static String message2 = " ";
	public static String message3 = " ";
	public static boolean msg1, msg2, msg3 = false; 
	
	public static void main(String[] args) {
	
		// set x coordinate for text
		String enterxtwo = "Enter x coordinate for text ";
		String xtwo = JOptionPane.showInputDialog(enterxtwo);
		
		x2 = Integer.parseInt(xtwo);
		
		//makes sure x isn't negative
		if(x2 < 0)
		{
			x2 = -x2;
		}
		
		// checks if x is negative, if not ask user for y value of text
		if(x2 >= 0)
		{
			String enterytwo = "Enter y coordinate for text ";
			String ytwo = JOptionPane.showInputDialog(enterytwo);
			y2 = Integer.parseInt(ytwo);
			
			//make sure y isn't negative
			if(y2 < 0)
				{
					y2 = -y2;
				}
			
		}
		
		// enter first line of text
		String enter1 = "Enter your first message ";
		String m1 = JOptionPane.showInputDialog(enter1);
		message1 = m1;
		
		if(message1 != null)
		{
			/* msg# booleans were created to prevent program from having an error
			 * by trying to write null
			 */
			msg1 = true;
			// enter second line of text
			String enter2 = "Enter your second message ";
			String m2 = JOptionPane.showInputDialog(enter2);
			message2 = m2;
			if(message2 != null)
			{
				msg2 = true;
				// enter third line of text
				String enter3 = "Enter your third message ";
				String m3 = JOptionPane.showInputDialog(enter3);
				message3 = m3;
				if(message3 != null)
				{
					msg3 = true;
				}
			}
		}

		// checks if user is done with text, moves on to image x & 
		if(x2 >= 0 && y2 >= 0 )
		{
			// set x for image
			String enterx = "Enter x coordinate for image ";
			String x = JOptionPane.showInputDialog(enterx);
			
			x1 = Integer.parseInt(x);
			
			//makes sure x isn't negative
			if(x1 < 0)
			{
				x1 = -x1;
			}
			
			// checks if x is under 0, if not ask for y coordinate
			if(x1 >= 0)
			{
				String entery = "Enter y coordinate for image ";
				String y = JOptionPane.showInputDialog(entery);
				y1 = Integer.parseInt(y);
				
				//make sure y isn't negative
				if(y1 < 0)
					{
						y1 = -y1;
					}
				
			}
		}
		
		MovingFigure figure = new MovingFigure();

	}
	
	public MovingFigure()
	
	{
		this.setSize(500,500);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	static void drawObjects(Graphics g, int xstart, int ystart)
	{
		// draw cactus
		g.setColor(Color.green);
		g.fillOval(xstart + 130, ystart + 95, 40, 40);
		g.fillRect(xstart + 130, ystart + 120, 40, 50);
		g.fillRect(xstart + 60, ystart + 145, 100, 40);
		g.fillRect(xstart + 80, ystart + 70, 40, 200);
		g.fillRect(xstart + 30, ystart + 115, 40, 50);
		g.fillOval(xstart + 32, ystart + 135, 40, 50);
		g.fillOval(xstart + 130, ystart + 135, 40, 50);
		g.fillOval(xstart + 30, ystart + 95, 40, 50);
		g.fillOval(xstart + 80, ystart + 45, 40, 50);
	}
	
	public void paint(Graphics g)
	{
		drawObjects(g, x1, y1);
		writeText((Graphics2D) g, x2,y2);
	}
	
	static void writeText(Graphics2D g, int startx, int starty)
	{
		g.setColor(Color.black);
		g.setFont(new Font("Forte", Font.CENTER_BASELINE, 25));
		if(msg1 == true)
		{
		g.drawString(message1, startx, starty);
		}
		if(msg2 == true)
		{
			g.drawString(message2, startx, starty+30);
		}
		if(msg3 == true)
		{
			g.drawString(message3, startx, starty+60);
		}
	}
}


