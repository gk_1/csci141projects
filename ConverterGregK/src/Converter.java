public class Converter 
{
	static int place = 1;
	static int counter = 0;
	static int exponent = 0;
	static int total = 0;
	static int tempval = 0;
	static String outString = "";

	// base 10 to octal

	public static String baseToOctal(int in) 
	{
		// Reset all variables in case they have already been used. This is used in all methods.
		place = 1;
		counter = 0;
		exponent = 0;
		total = 0;
		tempval = 0;
		outString = "";
		while (in != 0) 
		{
			// find the remainder of in/8. Add it to the outString
			outString = in % 8 + outString;
			// divide input by 8
			in = in / 8;
		}
		// reset input to 0. This is used in all methods.
		in = 0;
		return outString;
	}

	// base 10 to binary

	public static String baseToBinary(int in) 
	{
		place = 1;
		counter = 0;
		exponent = 0;
		total = 0;
		tempval = 0;
		outString = "";
		while (in != 0) {
			// find the remainder of input/2. Add it to the outString.
			outString = in % 2 + outString;
			// Divide input by 2
			in = in / 2;
		}
		in = 0;
		return outString;
	}

	// // binary to base 10

	public static String binaryToBase(int in) 
	{
		place = 1;
		counter = 0;
		exponent = 0;
		total = 0;
		tempval = 0;
		outString = "";
		while (in > 0) {
			// This while loop is used to count the number in the rightmost place.
			while ((in - counter) % 10 != 0 && in != 0) 
			{
				counter++;
			}
			// Tempval is the value of the base (2) raised to the current exponent, then multiplied by the counter.
			// The counter represents the current value in the rightmost place.
			tempval = (int) (Math.pow(2, exponent) * counter);
			// Calculate total value
			total = tempval + total;
			// Subtract added values from the input
			in -= counter;
			//Divide by 10 so (in -counter % 10) != 0 AKA the rightmost place isn't 0.
			in = in / 10;
			// increase exponent value
			exponent++;
			// reset counter
			counter = 0;
		}
		in = 0;
		return String.valueOf(total);
	}

	// octal to base 10

	public static String octalToBase(int in) 
	{
		place = 1;
		counter = 0;
		exponent = 0;
		total = 0;
		tempval = 0;
		outString = "";
		while (in > 0) {
			// This while loop is used to count the number in the rightmost place.
			while ((in - counter) % 10 != 0 && in != 0) 
			{
				counter++;
			}
			// Tempval is the value of the base (8) raised to the current exponent, then multiplied by the counter.
			// The counter represents the current value in the rightmost place.
			tempval = (int) (Math.pow(8, exponent) * counter);
			// Calculate total value
			total = tempval + total;
			// Subtract added values from the input
			in -= counter;
			//Divide by 10 so (in -counter % 10) != 0 AKA the rightmost place isn't 0.
			in = in / 10;
			// increase exponent value
			exponent++;
			// reset counter
			counter = 0;
		}
		in = 0;
		return String.valueOf(total);
	}

}
