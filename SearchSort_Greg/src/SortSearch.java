/**
 * Search and sorting class.
 * Has the methods for sorting and searching.
 * Selection sort is used for sorting, binary search is used for searching.
 * 
 * @author Greg Kline
 * 
 *
 */
import java.util.Arrays;

public class SortSearch {
	private int[] theData;

	/**
	 * Constructor
	 * Takes in array
	 * @param array
	 */
	public SortSearch(int[] array) {
		theData = array;
	}

	// Selection sort method. Used to sort data from the array.
	public void sort() {
		int smallIndex;
		int tempVal;
		for(int i=0;i<theData.length;i++)
		{
			smallIndex=i;
			//Arrow is the index of the value you are comparing to the smallest index's value
			for(int arrow =i+1;arrow< theData.length;arrow++)
			{
				if(theData[arrow] < theData[smallIndex]){
					smallIndex=arrow;
				}
			}
			//Swapping values
			tempVal=theData[i];
			theData[i] = theData[smallIndex];
			theData[smallIndex]=tempVal;
		}
	}
	/**
	 * Binary search method. 
	 * Takes in a int value to find.
	 * @param find
	 * @return
	 */
	public int binarySearch(int find) {
		int middle = (theData.length - 1) / 2;
		int high = theData.length - 1;
		int low = 0;
		for (int i = 0; i <= theData.length; i++) {
			if (theData[middle] == find) {
				System.out.println(find + "found at : " + middle);
				return middle;
			} else if (theData[high] == find) {
				return high;
			} else if (theData[middle] > find) {
				high = middle;
				middle = (high + low) / 2;
			} else if (theData[middle] < find) {
				low = middle;
				middle = (high + low) / 2;
			}
		}
		/*If the number does not exist in the array, the index is -1. This will affect the update method in main.
		 * -1 is chosen because negative indexes can not exist.
		 */
		if (theData[middle] != find) {
			middle = -1;
		}
		return middle;
	}
}
