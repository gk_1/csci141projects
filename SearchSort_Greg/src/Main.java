/**
 * Main class used for creation of GUI.
 * User input is taken in and used for sorting and searching.
 * 
 * @author Greg Kline
 * 
 * Project Due: April 7th, 2017
 */
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.*;

public abstract class Main extends JFrame {
	private static int[] data;
	private static int find;
	private static int checker;
	private static JLabel foundMessage = new JLabel("Waiting for user input...");
	

	public static void gui() {
		String startMessage = "Type your array in here. Use spaces to seperate #s";
		String noInput = "";
		String message1 = "Waiting for user...";
		// initialize GUI components
		JFrame sorter = new JFrame("Binary Search");
		JButton sortButton = new JButton("Press to sort");
		JButton searchButton = new JButton("Press to search");
		JTextArea arrayInput = new JTextArea(startMessage,5,15);
		JTextField dataInput = new JTextField("Input # to find", 10);
		
		/* Sort button. Calls selection sort and checks for errors.
		 * Takes in user input as string array, uses for loop to convert to int array.
		 */
		sortButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {	    	
		    	try{
		    	String[] rawNumbers = arrayInput.getText().split(" ");
		    	data = new int[rawNumbers.length];
		    	for(int i = 0; i < rawNumbers.length; i++)
		    	{
		    		// Found how to parse string array to int array on stackoverflow
		    		data[i] = Integer.parseInt(rawNumbers[i]);
		    	}
		    	System.out.println(Arrays.toString(data));
		    	SortSearch SortSearch = new SortSearch(data);
		    	SortSearch.sort();
		    	arrayInput.setText(Arrays.toString(data));
		    	}
		    	catch(java.lang.NumberFormatException ex)
		    	{
		    		arrayInput.setText("Make sure you are only inputting numbers.");
		    	}
		    }
		});
		

		/*Search button for finding a user inputed number.
		 * Calls binary search.
		 */
		searchButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) 
		    {
		    	try{
		    	String getFind = dataInput.getText();
		    	find = Integer.parseInt(getFind);
		    	 SortSearch SortSearch = new SortSearch(data);
		    	SortSearch.binarySearch(find);
		    	checker = SortSearch.binarySearch(find);
		    	System.out.println("Checker is: " + checker);
		    	update();
		    	}
		    	catch(java.lang.NumberFormatException ex){
		    		checker = -2;
		    		update();
		    	}
		    }
		});

		// Set up layout of GUI
		sorter.setLayout(new FlowLayout());
		sorter.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		sorter.add(arrayInput,BorderLayout.NORTH);
		sorter.add(dataInput,BorderLayout.SOUTH);
		sorter.add(searchButton,BorderLayout.SOUTH);
		sorter.add(sortButton,BorderLayout.SOUTH);
		sorter.add(foundMessage,BorderLayout.WEST);
		sorter.pack();
		sorter.setSize(500, 200);
		sorter.setVisible(true);

	}

	// Method to update GUI messages
	public static void update(){
		if(checker == -1)
		{
			foundMessage.setText("Number does not exist in the array.");
		}
		else if(checker == -2){
			foundMessage.setText("Either sort first, or only input numbers.");
		}
		else{
			foundMessage.setText("Number: " + find + " found at index : " + checker);
		}
	}
	
	public static void main(String[] args) {
		gui();
	}
}
