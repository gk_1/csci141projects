import java.util.concurrent.ThreadLocalRandom;
/**
 * Rolls a die and returns a random value between 1 and 6. 
 * 
 * @author Noah Nagel
 * @author Greg Kline
 */

public class PigDie {

	private int myTop;
	/**
	 * Constructor
	 */
	public PigDie()
	
	{
		
	}
	
	// Random number between 0 and 6 set equal to myTop, return myTop
	public int Roll()
	{		
	
		myTop = ThreadLocalRandom.current().nextInt(1, 7);
		return myTop;
	}

	/**
	 * Setters and getters
	 * @return
	 */
	public int getMyTop() {
		return myTop;
	}

	public void setMyTop(int myTop) {
		this.myTop = myTop;
	}
	
}
