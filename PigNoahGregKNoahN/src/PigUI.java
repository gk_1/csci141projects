import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
/**
 * Creates GUI.
 * Creation of buttons, images, and labels using multiple panels on one frame.
 * There are two panes that ask for a player name and maximum score.
 * 
 * Used Dr. Eckroth's website for help in creating GUI.
 * 
 * @author Greg Kline
 * @author Noah Nagel
 */
public class PigUI extends JFrame{
	private static int myMaxPoints = 0;
	
	private static ImageIcon myBlank1 = new ImageIcon("images/none.png");
	private static ImageIcon myBlank2 = new ImageIcon("images/none.png");
	private static JLabel myDiceLabel1 = new JLabel(myBlank1);
	private static JLabel myDiceLabel2 = new JLabel(myBlank2);
	private static JLabel myPlayerName = new JLabel();
	private JTextPane myPlayerRoundScore = new JTextPane();
	private JTextPane myPlayerTotalScore = new JTextPane();

	private static JLabel myInfoLabel = new JLabel();
	private static JLabel myComputerName = new JLabel();
	private JTextPane myComputerRoundScore = new JTextPane();
	private JTextPane myComputerTotalScore = new JTextPane();

	private JButton myRollButton;
	private JButton myPassButton;
	private JButton myResetButton;
	private PigEngine myEngine;
	
	/**
	 * Constructor
	 */
	public PigUI()
	{
		gui();
	}

	public void gui(){
		JFrame frame = new JFrame("Pig");
		
		String maxpointmessage = "Enter a number between 50 and 200 ";
		String maxPoints = JOptionPane.showInputDialog(maxpointmessage);
		
		String nameAsk = "What is your name? ";
		String name = JOptionPane.showInputDialog(nameAsk);
		myPlayerName.setText(name);
		
		// checks if max points is a letter, if it is set max points to 50 and display an error message
				try{
				myMaxPoints = Integer.parseInt(maxPoints);
				}
				catch(java.lang.NumberFormatException ex){
					JOptionPane.showMessageDialog(null, "Invalid number entered, setting max points to 50.", "Error", JOptionPane.ERROR_MESSAGE);
					myMaxPoints = 50;
				}
				
				// checks if the user inputs a number greater than 50 or less than 20, if so maxPoints is set to 50
				if(myMaxPoints< 50 || myMaxPoints > 200)
				{
					JOptionPane.showMessageDialog(null, "Invalid number entered, setting max points to 50.", "Error", JOptionPane.ERROR_MESSAGE);
					myMaxPoints = 50;
				}
		myEngine = new PigEngine(myMaxPoints, this);
		myEngine.playerOne = new pigPlayer(name);
		myEngine.playerTwo = new pigPlayer("Computer");
		
		System.out.println(myEngine.playerOne.getMyName() + "'s score is " + myEngine.playerOne.getMyRoundScore());
		System.out.println(myEngine.playerTwo.getMyName() + "'s score is " + myEngine.playerTwo.getMyRoundScore());
		
		// Create dice panel, initialize roll button
		JPanel myDiceDisplay = new JPanel();
		myDiceDisplay.setLayout(new FlowLayout());
		myRollButton = new JButton("Roll");
		
		//The roll button
		myRollButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//First check if the game is over yet...
				if (!myEngine.getIsMatchOver()) {
				//Doesn't work
				myInfoLabel.setText("You are rolling the dice...");		
				myEngine.turnRoll();
				//Update this UI
				updateDisplays();
				}
			}
		});
		
		//Starting message and start up game
		myInfoLabel.setText("Welcome to Pig! It is your turn. Please roll the dice to start the game");
		myEngine.startGame();
		
		//The pass button
		myPassButton = new JButton("Pass");
		myPassButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Again check if game is over
				if (!myEngine.getIsMatchOver()) {
					//Check if it's the player's turn
					if (myEngine.playerOne.isMyTurn())
					{
						myEngine.turnPass();
						updateDisplays();
					}
					else
					{
					myInfoLabel.setText("You can't do that! It's not your turn yet! Click the roll button instead.");
					}
				}
			}
		});

		//The reset button
		myResetButton = new JButton("Reset");
		myResetButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				myEngine.startGame();
				updateDisplays();
				myInfoLabel.setText("Reset the game. Good luck!");
			}
		});

		//Adding things to the frame and setting up the UI
		
		//Initializing JPanels
		frame.setLayout(new BorderLayout());
		JPanel myButtonsPanel = new JPanel();
		JPanel myPlayerOnePanel = new JPanel();
		JPanel myComputerPanel = new JPanel();
		JPanel myInfoPanel = new JPanel();
		
		//Setting layouts
		myPlayerOnePanel.setLayout(new FlowLayout());
		myComputerPanel.setLayout(new FlowLayout());
		myInfoPanel.setLayout(new FlowLayout());
		myButtonsPanel.setLayout(new FlowLayout());
		
		//Adding the buttons
		myButtonsPanel.add(myRollButton);
		myButtonsPanel.add(myPassButton);
		myButtonsPanel.add(myResetButton);
		
		//Setting up the left side for player one
		myPlayerOnePanel.add(myPlayerName);
		myPlayerOnePanel.add(myPlayerRoundScore);
		myPlayerOnePanel.add(myPlayerTotalScore);
		
		//Setting up the right side for player two
		myComputerPanel.add(myComputerName);
		myComputerPanel.add(myComputerRoundScore);
		myComputerPanel.add(myComputerTotalScore);
		
		//Info label
		myInfoPanel.add(myInfoLabel);
		
		//Adding all the above to the frame
		frame.add(myButtonsPanel, BorderLayout.NORTH);
		frame.add(myDiceDisplay, BorderLayout.CENTER);
		frame.add(myPlayerOnePanel, BorderLayout.WEST);
		frame.add(myComputerPanel, BorderLayout.EAST);
		frame.add(myInfoPanel, BorderLayout.SOUTH);
		
		//Giving the dice icons to the display
		myDiceDisplay.add(myDiceLabel1);
		myDiceDisplay.add(myDiceLabel2);
		
		//Exit on close
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//Finalize UI setup

		frame.pack();
		frame.setSize(800, 250);		
		frame.getContentPane().setBackground(Color.green);		
		frame.setVisible(true);
	}
	
	/**
	 * Used to keep the GUI up to date
	 */
	public void updateDisplays()
	{
		myPlayerRoundScore.setText ("Round score is: " + String.valueOf(myEngine.playerOne.getMyRoundScore()));
		myPlayerTotalScore.setText ("Total score is: " + String.valueOf(myEngine.playerOne.getMyTotalScore()));
		
		myComputerRoundScore.setText ("Round score is: " + String.valueOf(myEngine.playerTwo.getMyRoundScore()));
		myComputerTotalScore.setText ("Total score is: " + String.valueOf(myEngine.playerTwo.getMyTotalScore()));
		
		myInfoLabel.setText (myEngine.getMyInfoMessage());


		// for loop for determining which image to assign to the die
		for (int i = 0; i <= 6; i++)
		{
			if (myEngine.myPigDie1.getMyTop() == i) {
				ImageIcon newImage = new ImageIcon("images/" + i + ".png");
				myDiceLabel1.setIcon(newImage);
			}
		}
		
		for (int i = 0; i <= 6; i++)
		{
			if (myEngine.myPigDie2.getMyTop() == i) {
				ImageIcon newImage = new ImageIcon("images/" + i + ".png");
				myDiceLabel2.setIcon(newImage);
			}
		}
	}
}
	