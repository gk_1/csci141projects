/**
 * The game engine that handles turn rolls, keeping track of scores, processing turns, and 
 * finding the winner. Implements runnable.
 * 
 * @author Noah Nagel
 * @author Greg Kline
 *
 */
public class PigEngine implements Runnable{

	private int myMaxPerRound;

	private String myInfoMessage;
	private boolean matchIsOver;

	PigDie myPigDie1;
	PigDie myPigDie2;

	pigPlayer playerOne;
	pigPlayer playerTwo;

	
	pigPlayer currentPlayer;
	pigPlayer otherPlayer;
	
	PigUI myUI;

	/** Constructor, sets up cleared variables for the game to get started
	 * 
	 * @param maxPerRound
	 * @param newUI
	 */
	public PigEngine(int maxPerRound, PigUI newUI)
	{
		setMyInfoMessage("");
		myMaxPerRound = maxPerRound;
		myUI = newUI;
		matchIsOver = false;
	}
	public void startGame()
	{

		matchIsOver = false;
		
		playerOne.resetRoundScore();
		playerOne.resetTotalScore();
		playerTwo.resetRoundScore();
		playerTwo.resetTotalScore();
		
		myPigDie1 = new PigDie();
		myPigDie2 = new PigDie();
		playerOne.setMyTurn(true);
		currentPlayer = playerOne;
		otherPlayer = playerTwo;
	}
	
	//Hitting the roll button, may be called automatically from AI
	public void turnRoll()
	{
		setMyInfoMessage(currentPlayer.getMyName() + " is rolling the dice...");
		myUI.updateDisplays();
		myPigDie1.Roll();			
		myPigDie2.Roll();
		currentPlayer.addRoundScore(myPigDie1.getMyTop(), myPigDie2.getMyTop());
		System.out.println("Player " + currentPlayer.getMyName() + " rolled a " + myPigDie1.getMyTop() + " and a " + myPigDie2.getMyTop());
		System.out.println("Player " + currentPlayer.getMyName() + "'s current round score is " + currentPlayer.getMyRoundScore());
		myUI.updateDisplays();
		//Because the tops of the dice can affect what happens next, process them in ProcessTurn
		processTurn();
	}

	/**Basic AI turn processing
	 * Pivots actions around "20". Takes turn accordingly
	 */
	
	public void aiLogic()
	{
		if (playerTwo.isMyTurn() == true)
		{
			myUI.updateDisplays();
			myUI.updateDisplays();
			turnRoll();	
			if (playerTwo.getMyRoundScore() >= 20)
			{
				System.out.println("Computer passing");
				setMyInfoMessage("Computer has decided to pass their turn");
				myUI.updateDisplays();
				turnPass();
			}
			
			if (playerTwo.getMyRoundScore() < 20)
			{
				if (playerTwo.isMyTurn() == true)
				{
				System.out.println("Computer rolling to get to twenty");
				setMyInfoMessage("Computer is rolling again...");
				myUI.updateDisplays();
				turnRoll();
				}
			}
		}
	}
	
	//Passes turn to other player. This can be voluntary or forced by the engine if the turn was a one. 
	public void turnPass()
	{
		setMyInfoMessage(currentPlayer.getMyName() + " has passed their turn. Giving control to " + otherPlayer.getMyName() + ".");
		myUI.updateDisplays();
		System.out.print(currentPlayer.getMyName() + " has passed their turn. Giving control to " + otherPlayer.getMyName() + ".");
		//Actually solidify round points into total score because of voluntary passing
		currentPlayer.addTotalScore();
		System.out.println(" Player " + currentPlayer.getMyName() + "'s total score is " + currentPlayer.getMyTotalScore());
		switchTurn();
		
		if (otherPlayer == playerTwo)
		{
			aiLogic();
		}
		myUI.updateDisplays();
	}
	
	
	public void processTurn()
	{
		//Creating variables only in this method with the top to not interfere with the actual top
		int die1Top = myPigDie1.getMyTop();
		int die2Top = myPigDie2.getMyTop();

		//For this method, figuring out who just went
		if (playerOne.isMyTurn() == true)
		{
			currentPlayer = playerOne;
			otherPlayer = playerTwo;
		}
		else
		{
			currentPlayer = playerTwo;
			otherPlayer = playerOne;
		}
		
		if (currentPlayer == playerOne)
		{
		setMyInfoMessage(currentPlayer.getMyName() + " has rolled a " + die1Top + " and a " + die2Top + ". You may roll again, or pass the dice to " + playerTwo.getMyName() + " to add your round score to the total score.");
		}
		else
		{
		setMyInfoMessage(currentPlayer.getMyName() + " has rolled a " + die1Top + " and a " + die2Top + ". Please roll again for them, they still have no hands.");
		}
//		
		boolean computerTryAgain = true;

		//If two ones, reset round and total and switch
		if ((die1Top == 1) && (die2Top == 1)){
			setMyInfoMessage("Bad luck! Rolled two ones. Total score reset for "  + currentPlayer.getMyName() + " and giving turn to " + otherPlayer.getMyName());
			myUI.updateDisplays();
			System.out.println("Bad luck! Rolled two ones. Total score reset for "  + currentPlayer.getMyName() + " and giving turn to " + otherPlayer.getMyName());
			computerTryAgain = false;
			currentPlayer.resetTotalScore();
			currentPlayer.resetRoundScore();
			switchTurn();
		}

		//If one one, reset round score and switch
		else if ((die1Top == 1) || (die2Top == 1))
		{
			setMyInfoMessage("Rolled a one. Round score reset for "  + currentPlayer.getMyName() + " and giving turn to " + otherPlayer.getMyName());
			myUI.updateDisplays();
			System.out.println("Rolled a one. Round score reset for "  + currentPlayer.getMyName() + " and giving turn to " + otherPlayer.getMyName());
			currentPlayer.resetRoundScore();
			computerTryAgain = false;
			switchTurn();
			
		}
		
		if (currentPlayer == playerTwo)
		{
			if (computerTryAgain)
			{
				if (playerTwo.getMyRoundScore() < 20)
				{
					System.out.println("Computer should roll again");

				}
				else
				{
					turnPass();
				}
			}
		}
		myUI.updateDisplays();
				
	}
	
	//Checking each player's local isMyTurn boolean to decide who went before, toggle for both players, set currentPlayer and 
	//otherPlayer for the engine to work smoothly
	public void switchTurn()
	{
		if ((playerOne.getMyTotalScore() >= myMaxPerRound) || (playerTwo.getMyTotalScore() >= myMaxPerRound))
		{
			if (playerOne.getMyTotalScore() >= myMaxPerRound)
			{
				setMyInfoMessage(playerOne.getMyName() + " has won! Please reset the game to start a new match.");
			}
			
			if (playerTwo.getMyTotalScore() >= myMaxPerRound)
			{
				setMyInfoMessage(playerTwo.getMyName() + " has won! Please reset the game to start a new match.");
			}
			matchIsOver = true;
			}

		myUI.updateDisplays();
		if (playerOne.isMyTurn() == true)
		{
			playerOne.setMyTurn(false);
			playerTwo.setMyTurn(true);		
			currentPlayer = playerTwo;
			otherPlayer = playerOne;
			return;
		}
		
		if (playerTwo.isMyTurn() == true)
		{
			playerTwo.setMyTurn(false);
			playerOne.setMyTurn(true);
			currentPlayer = playerOne;
			otherPlayer = playerTwo;
			return;
		}
	}

	public String getMyInfoMessage() {
		return myInfoMessage;
	}

	public boolean getIsMatchOver() {
		return matchIsOver;
	}
	
	public void setMyInfoMessage(String myNewInfoMessage) {
		this.myInfoMessage = myNewInfoMessage;
	}
	
	public void callRoll() {
		turnRoll();
	}

	@Override
	public void run() {

	}
}


