/**
 * Handles player and computer's scores. Also is responsible for the player name.
 * Getters are used to get total scores and round scores. Setter is used to set player turns.
 * 
 * 
 * @author Greg Kline
 * @author Noah Nagel
 */
public class pigPlayer {

	private String myPlayerName;
	private int myTotalScore;
	
	private int myRoundScore;
	
	private boolean myTurn;
	
	public pigPlayer(String myName)
	{
		myPlayerName = myName;
	}
	
	public void addRoundScore(int die1Top, int die2Top)
	{
		myRoundScore += (die1Top + die2Top);
	}
	
	public void addTotalScore()
	{
		myTotalScore += myRoundScore;
		myRoundScore = 0;
	}

	public boolean isMyTurn() {
		return myTurn;
	}

	public void setMyTurn(boolean myTurn) {
		this.myTurn = myTurn;
	}
	
	
	public String getMyName() {
		return myPlayerName;
	}
	
	public int getMyRoundScore() {
		return myRoundScore;
	}
	
	public int getMyTotalScore() {
		return myTotalScore;
	}
	
	public void resetTotalScore() {
		myTotalScore = 0;
		myRoundScore = 0;
	}
	
	public void resetRoundScore() {
		myRoundScore = 0;
	}
	
	
}