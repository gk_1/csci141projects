/**
 * A dice game called pig that provides the user with a GUI in which they can 
 * chose to roll dice, pass their turn, and keep track of their scores.
 * It is possible to win or lose. 
 * The program is broken up into 5 different classes: PigMain, PigEngine, PigUI, PigPlayer, PigDie, and PigPlayer.
 * 
 * @author Greg Kline
 * @author Noah Nagel
 * 
 * Project Due: March 24th, 2017
 */
public class PigMain {

	public static void main(String[] args) {
		PigUI myPigUI = new PigUI();
	}

}
