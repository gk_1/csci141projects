
package rings;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

public class theRings extends JFrame{

	public static void main(String[] args) 
	{
		theRings rings = new theRings();
	}
	public theRings(){	
		this.setSize(500,300);
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	public void paint(Graphics ringGraphics){
		ringGraphics.setColor(Color.CYAN);
		ringGraphics.drawOval(45,100,100,100);
		ringGraphics.setColor(Color.YELLOW);
		ringGraphics.drawOval(100, 150, 100, 100);
		ringGraphics.setColor(Color.BLACK);
		ringGraphics.drawOval(155, 100, 100, 100);
		ringGraphics.setColor(Color.GREEN);
		ringGraphics.drawOval(210, 150, 100, 100);
		ringGraphics.setColor(Color.RED);
		ringGraphics.drawOval(265, 100, 100, 100);
		
	}
}


